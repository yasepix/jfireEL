package com.jfirer.jfireel.template.execution;

public interface InvokerOnExecute
{
    void invoke(Object value);
}
